add_executable(units_encode_file units_encode_file.cxx)

# These are added so that we can include units header files
# containing compiler and system inspection macros:
target_include_directories(units_encode_file
  PRIVATE
    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/../..>
    $<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}/../..>
)

#include "units/System.h"
#include "units/json/jsonUnits.h"

#include <iostream>

int testJSON(int argc, char* argv[])
{
  bool ok = true;
  // This tests deserialization from Data.json:
  auto sys = units::System::createWithDefaults();

  nlohmann::json jj;
  jj = sys;

  std::string spec = jj.dump(2);

  auto sys2 = units::System::createFromSpec(spec);

  // Compare the source and duplicated System instances:
  std::cout << "Round trip deserialization and serialization produced Systems with:\n";
  std::cout << "  Prefixes " << sys->m_prefixes.size() << " vs " << sys2->m_prefixes.size() << "\n";
  if (sys->m_prefixes.size() != sys2->m_prefixes.size())
  {
    ok = false;
  }
  else
  {
    // Are all the source prefixes present in the output?
    for (const auto& pp : sys->m_prefixes)
    {
      if (sys2->m_prefixes.find(pp.first) == sys2->m_prefixes.end())
      {
        ok = false;
        std::cerr << "  Missing prefix " << pp.first.data() << "\n";
      }
    }
  }

  std::cout << "  Dimensions " << sys->m_dimensions.size() << " vs " << sys2->m_dimensions.size() << "\n";
  if (sys->m_dimensions.size() != sys2->m_dimensions.size())
  {
    ok = false;
  }
  else
  {
    // Are all the source dimensions present in the output?
    for (const auto& dd : sys->m_dimensions)
    {
      if (sys2->m_dimensions.find(dd.first) == sys2->m_dimensions.end())
      {
        ok = false;
        std::cerr << "  Missing dimension " << dd.first.data() << "\n";
      }
    }
  }

  std::cout << "  Units " << sys->m_units.size() << " vs " << sys2->m_units.size() << "\n";
  if (sys->m_units.size() != sys2->m_units.size())
  {
    ok = false;
  }

  if (!ok)
  {
    std::cerr << "Errors found. Output unit system spec:\n" << spec << "\n";
  }
  else
  {
    // Are all the source units present in the output?
    for (const auto& uu : sys->m_units)
    {
      if (sys2->m_unitsByName.find(uu->m_name) == sys2->m_unitsByName.end())
      {
        ok = false;
        std::cerr << "  Missing unit " << uu->m_name.data() << "\n";
      }
    }
  }

  return ok ? 0 : 1;
}

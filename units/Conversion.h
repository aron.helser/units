#ifndef units_Conversion_h
#define units_Conversion_h

#include "units/Unit.h"
#include "units/Measurement.h"

namespace units
{

struct Conversion;
struct Unit;

/// A base class for conversion rules.
///
/// Rules are invertible functions that transform a measurement containing a "source" unit
/// into one that contains the "target" unit. Note that both the source and the target
/// may be composite.
///
/// The source and target units must have identical dimensions.
///
/// A rule is provided with a pointer to a context that includes, at a minimum,
/// + whether the rule is to applied in a forward (from source to target unit)
///   or inverse (from target to source) sense.
/// + the exponent to apply to the conversion function (i.e., the number of
///   repeated evaluations to apply).
struct UNITS_EXPORT Rule : std::enable_shared_from_this<Rule>
{
  struct Context
  {
    Context()
      : forward(true)
      , exponent(1.0)
    {}

    Context(bool fwd, double exp)
      : forward(fwd)
      , exponent(exp)
    {}

    virtual ~Context() = default; // Force this struct to be polymorphic

    bool forward;
    double exponent;
  };
  using Evaluator = std::function<double(double)>;

  /// Given a \a conversion (general rule) and a \a context (specific to a single
  /// Measurement), return a function that applies the Rule's transform to an
  /// arbitrary value.
  ///
  /// The \a context may be ephemeral, so if a rule's evaluate() method needs data
  /// from the \a context, it should copy – rather than reference – data as needed.
  /// The \a conversion is not ephemeral and may be referenced by the returned
  /// function.
  virtual Evaluator evaluator(const Conversion& conversion, const Context& context) const = 0;

  /// Apply a Rule's transform to a measurement \a mm.
  ///
  /// Subclasses need not override this method; beyond creating and
  /// invoking an evalator, it also modifies the units of \a mm.
  /// Rules that need to change how units of a measurement are
  /// transformed may override but Rules that effectively divide by
  /// the source unit and multiply by the target unit (for forward
  /// contexts and vice-versa for inverse contexts) need not.
  virtual void transform(Measurement& mm, const Conversion& conversion, const Context& context);
};

/// Transform units that are linear factors of one another.
///
/// Note that this implies the two scales share the same origin along their shared dimension.
struct UNITS_EXPORT FactorRule : Rule
{
  FactorRule(double factor);
  Evaluator evaluator(const Conversion& conversion, const Context& context) const override;
  double m_factor{ 1. };
};

/// Transform units that are linear factors of one another with different origins.
///
/// Note that when a measurement has composite units (including a single unit raised
/// to a non-unit exponent), the OffsetRule assumes the value is a **relative value**
/// (i.e., a difference between two values along the axis) and thus ignores the
/// differnce in origins between the source and target units.
/// An example of this in practice is thermal expansion coefficients (such as mm / degF);
/// because the unit does not involve solely the temperature with a unit exponent,
/// converting a measurement from "mm/degF" to "mm/K" does **not** shift the measurement's
/// value by -469.67 before multiplying by the scale factor.
struct UNITS_EXPORT OffsetRule : FactorRule
{
  OffsetRule(double factor, double offset);
  Evaluator evaluator(const Conversion& conversion, const Context& context) const override;
  double m_offset{ 0. };

  /// The OffsetRule evaluator will dynamically cast its \a context
  /// to this subclass of Rule::Context and, if successful, use
  /// this to determine whether the value being converted is
  /// absolute or relative.
  struct Context : Rule::Context
  {
    bool absolute{ false };
  };
};

/// Transform units that are logarithmic factors of one another.
///
/// The only examples of this type of transform I know of are Bels (as in decibels, dB)
/// or Richter-scale measurements which are not currently supported as it is unclear
/// how best to do so. In theory, any unit can be transformed by Bels (not just sound
/// pressure levels). But then it would be necessary for *every* unit to have a Bel-scale
/// equivalent and that equivalent would need a unique name.
struct UNITS_EXPORT LogRule : Rule
{
  LogRule(double factor, double base);
  Evaluator evaluator(const Conversion& conversion, const Context& context) const override;
  double m_factor{ 1. };
  double m_base{ 10. };
};

/// Represent a potential unit transformation.
///
/// These objects are owned by a units::System and in turn own a Rule
/// that can be applied to either measurements or values in bulk.
struct UNITS_EXPORT Conversion : std::enable_shared_from_this<Conversion>
{
  Conversion(Unit from, Unit to, const std::shared_ptr<Rule>& rule);
  void apply(Measurement& m, bool forward, double exponent) const;
  void apply(Measurement& m, const Rule::Context& context) const;
  Rule::Evaluator evaluator(const Rule::Context& context) const;

  Unit m_source;
  Unit m_target;
  std::shared_ptr<Rule> m_rule;
};

} // namespace units

#endif // units_Conversion_h

#ifndef units_grammar_Functions_h
#define units_grammar_Functions_h

#include "units/grammar/Action.h"

namespace units
{
namespace grammar
{

bool UNITS_EXPORT parseUnits(const std::string& input, measurement_state& state);

} // namespace grammar
} // namespace units


#endif // units_grammar_Functions_h

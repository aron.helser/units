#include "units/Measurement.h"

#include <iostream>

namespace units
{

Measurement::Measurement(double value, Unit units)
  : m_value(value)
  , m_units(units)
{
}

void Measurement::dump() const
{
  std::cout << *this << "\n";
}

} // namespace units

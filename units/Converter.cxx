#include "units/Converter.h"

#include "units/Conversion.h"

namespace units
{

Converter::Converter(Unit from, Unit to)
  : m_source(from)
  , m_target(to)
{
}

Measurement Converter::transform(const Measurement& measurement) const
{
  Measurement result;
  if (measurement.m_units != m_source)
  {
    return result;
  }

  result.m_value = this->transform(measurement.m_value);
  result.m_units = m_target;
  return result;
}

double Converter::transform(double value) const
{
  double result = value;
  for (const auto& transform : m_transforms)
  {
    result = transform(result);
  }
  result *= m_scaling;
  return result;
}

} // namespace units

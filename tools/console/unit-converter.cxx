#include "units/System.h"

#include <iostream>
#include <string>

int main()
{
  auto sys = units::System::createWithDefaults();
  while (1)
  {
    std::cout << "You have: ";
    std::cout.flush();
    std::string source;
    std::getline(std::cin, source);
    if (source.empty())
    {
      break;
    }
    bool didParse;
    auto measurement = sys->measurement(source, &didParse);
    if (!didParse)
    {
      std::cerr << "Error: Could not parse \"" << source << "\".\n\n";
      continue;
    }

    std::cout << "You want: ";
    std::cout.flush();
    std::string target;
    std::getline(std::cin, target);

    auto unit = sys->unit(target, &didParse);
    if (!didParse)
    {
      std::cerr << "Error: Could not parse \"" << target << "\".\n\n";
      continue;
    }

    bool didConvert;
    auto result = sys->convert(measurement, unit, &didConvert);
    if (!didConvert)
    {
      std::cerr << "Could not convert " << measurement << " to " << unit << ".\n\n";
      continue;
    }

    std::cout << "\n  = " << result.m_value << " " << target << "\n";
  }

  return 0;
}

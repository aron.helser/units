# Install a package targets file
#
# Setup the exports for the library when used from an installed location
install(
  EXPORT ${PROJECT_NAME}
  DESTINATION ${units_INSTALL_CONFIG_DIR}
  FILE ${PROJECT_NAME}Targets.cmake
)

export(EXPORT ${PROJECT_NAME} FILE "${PROJECT_BINARY_DIR}/${units_INSTALL_CONFIG_DIR}/${PROJECT_NAME}Targets.cmake")

set(units_cmake_dir "${CMAKE_CURRENT_LIST_DIR}")
set(units_cmake_build_dir "${CMAKE_BINARY_DIR}/${units_INSTALL_CONFIG_DIR}")
set(units_cmake_destination "${units_INSTALL_CONFIG_DIR}")
include(unitsInstallCMakePackage)

set(units_MODULE_DIR "${PROJECT_SOURCE_DIR}/cmake")
set(units_CONFIG_DIR "${PROJECT_BINARY_DIR}")
configure_file(
  ${PROJECT_SOURCE_DIR}/cmake/${PROJECT_NAME}Config.cmake.in
  ${units_INSTALL_CONFIG_DIR}/${PROJECT_NAME}Config.cmake
  @ONLY)

# Create an install package configuration file
#
# Setup the config file for exports that stores what other thirdparty
# packages we need to search for ( MOAB, Remus, etc ) for when using the
# install version of units

# If we are performing a relocatable install, we must erase the hard-coded
# install paths we set in units_prefix_path before constructing the install
# package configuration file
if (units_RELOCATABLE_INSTALL)
  set(units_prefix_path)
endif()

set(units_MODULE_DIR "\${CMAKE_CURRENT_LIST_DIR}")
set(units_CONFIG_DIR "\${CMAKE_CURRENT_LIST_DIR}")
configure_file(
  ${PROJECT_SOURCE_DIR}/cmake/${PROJECT_NAME}Config.cmake.in
  ${PROJECT_BINARY_DIR}/CMakeFiles/${PROJECT_NAME}Config.cmake
  @ONLY)

install (FILES ${PROJECT_BINARY_DIR}/CMakeFiles/${PROJECT_NAME}Config.cmake
         DESTINATION ${units_INSTALL_CONFIG_DIR})

